# ccgrade: A codecheck homework grader for your course

`ccgrade` allows you to grade [codecheck]()-submitted homework for a course of students. This script is an adaptation of a similar script written for [codecheck homework submitted on Instructure Canvas](http://canvas-codecheck-grading.readthedocs.org/en/latest/index.html).


# Initial setup

## Python

Python needs to be installed on your system. You can download Python
at the [official download page](http://www.python.org/download/).
They should be compatible with any Python 2.7.x and Python 3.x
versions. The scripts were created using Python 2.7.5. I've tested
these scripts with Python 3.3 as well, and they seemed to work just
fine.

## JDK tools

You need to have the JDK installed and the command-line tools on your path, because the script uses the `jarsigner` command to verify codecheck files. On Windows, refer to [instructions at Oracle’s documentation](http://docs.oracle.com/javase/7/docs/webnotes/install/windows/jdk-installation-windows.html#path) to set up your command-line path to have the Java tools ready. On Linux and OS X, the tools should automatically be found on your path if you have Java already.

## Installation

To get the scripts onto your computer, clone this repository:

	git clone https://bitbucket.org/danielmai/ccgrade.git

Later if you want to update the scripts with the latest changes, you may run a `git pull`.

# Directory structure

`ccgrade` assumes the directory structure of your course is like the following:

	coursedirectory
	├── StudentName1
	│   └── hw1.zip
	├── StudentName2
	│   └── hw1.zip
	├── StudentName3
	│   └── hw1.zip
	├── StudentName4
	│   └── hw1.zip
	├── StudentName5
	│   └── hw1.zip
	├── StudentName6
	│   └── hw1.zip
	├── StudentName7
	│   └── hw1.zip
	├── StudentName8
	│   └── hw1.zip
	└── StudentName9
		└── hw1.zip

That is, your course directory is made up of directories for each student that is named after him or her, and each student has the homework file that you want to grade in their respective student directories according to who submitted it.

# Example usage

	$ python ccgrade.py homework.zip path/to/coursedirectory

`ccgrade` needs the name of the `homework.zip` file that you want grade and create a homework report for as well as the path to the course directory with the students' homework submissions.

Once you run the command, you'll be prompted for the number of parts (i.e., the number of codecheck files) there are in this assignment and for the total points this assignment is worth. These two bits of information are used in calculating the total score. Here's a sample scenario where there are 2 codecheck files for a `hw1.zip` assignment that's worth 10 points:

	$ python ccgrade.py hw1.zip path/to/course
	> How many parts are in hw1.zip? 2
	> What's the maximum possible score for hw1.zip? 10

If you'd rather not deal with interactive prompts, you can pass this information as command line arguments using the `--number-parts` and `--max-score` options. The above example may be written at the command line like so:

	$ python ccgrade.py hw1.zip path/to/coursedirectory --number-parts 2 --max-score 10

# Try out the sample

There is a sample directory in the repository with a sample course directory named `sample-course`. In it, there are 30 student directories, each with two homework files: `hw1.zip` and `hw2.zip`. They each have two codecheck files in them. You can try out the script on this directory and see how it works with these commands

	$ python ccgrade.py hw1.zip path/to/repo/sample/sample-course --number-parts 2

	$ python ccgrade.py hw2.zip path/to/repo/sample/sample-course --number-parts 2

# More Options

- `--no-verify`
	- Use this option when you want to forgo `jarsigner` verification before extracting codecheck submission files.
- `--debug`
	- Use this option when you would like more information to be outputted to the screen than normal.
- `--log`
	- Use this option to write debugging information to a log file called `ccgrade.log` in the script directory.


If you ever forget, you can use the `--help` option for a quick reference.

	usage: ccgrade.py [-h] [-d] [-n NUMBER_PARTS] [-m MAX_SCORE] [--no-verify]
					  [--no-checks] [--log]
					  homework_file course_path

	positional arguments:
	  homework_file         The name of the homework file. Must match a homework
							file ready to be graded in the student directories.
							e.g., hw1.zip
	  course_path           the directory the student's homework submissions.

	optional arguments:
	  -h, --help            show this help message and exit
	  -d, --debug           print more info to stdout for debugging purposes
	  -n NUMBER_PARTS, --number-parts NUMBER_PARTS
							provide the number of parts for this assignment
	  -m MAX_SCORE, --max-score MAX_SCORE
							The maximum score possible for this assignment
	  --no-verify           skip jarsigner verification and unarchive all
							submission files
	  --log                 Send log of the script execution to a file
							'grade.log'.

# Homework report

A homework report is a large txt document that a grader can see the scores for each student's homework submission and make comments on them. A grader can also modify the score on this document as needed.

On the top of the homework report you will find general information about the assignment, such as the assignment name, maxmimum possible score, average of the scores, and standard deviation of the scores.

	:assignment name: hw1.zip
	:max score possible: 10
	:average: 10.0
	:standard deviation: 0.0
	--------
	...

# Grading a student's submission

In the homework report, you will see a section for each student in the course (inferred by the student directories). Here's an example of a student in a homework report:

	:name: Student1
	:total score: 20/20 (100.00%)
	:comment: 





	:notes and score changes: 





	--------

The `:comment:` section is used for the comments to each student. The `:notes and score changes:` section is used for notes by the grader and to modify the score in `:total score:`. It's best to use this section to change the score when the report is parsed later rather than touch the `:total score:` so you know what score `ccgrade` calculated and what steps you took as a grader to modify the score. Example usage of changing the score can be found on the documentation page for the Canvas report scripts [here](http://canvas-codecheck-grading.readthedocs.org/en/latest/usage.html#grading-structure).
# License

This software is licensed under the GNU General Public License v3. See LICENSE for more information.
