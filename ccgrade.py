#!/usr/bin/env python
"""ccgrade: Grade codecheck files organized in a course directory of students."""

from __future__ import absolute_import
from contextlib import contextmanager
import argparse
import codecs
from collections import OrderedDict
from fractions import Fraction
import logging
import math
import os
import re
import shutil
import subprocess
import sys
import zipfile
import pprint

from report_keys import (ASSIGNMENT_NAME_KEY,
                         MAX_KEY,
                         AVERAGE_KEY,
                         STD_DEV_KEY,
                         NAME_KEY,
                         SCORE_KEY,
                         COMMENT_KEY,
                         EXTRA_KEY,
                         SEPARATOR)

__version__ = "0.1.0"
__author__ = "Daniel Mai (daniel@danielmai.net)"
__copyright__ = "(C) 2014 Daniel Mai"
__contributors__ = ["Jon Pearce"]

encoding = "UTF-8"

# Python 2 compatibility
if sys.version[0] == "2": input = raw_input

# the list of submission filenames that weren't signed
unsigned_submissions = []

# possible codecheck file extensions
codecheck_exts = ('.jar', '.signed.zip', '.zip')

# A mapping of the total scores for this homework.
# The mapping goes as follows:
#   student-directory-name: score <type 'int'>

student_info = OrderedDict()

# Used with 'with' blocks
# Source: http://stackoverflow.com/a/13847807
@contextmanager
def pushd(new_dir):
    """Temporarily makes the new_dir the current directory,
    and then returns to the previous directory when finished."""
    previous_dir = os.getcwd()
    os.chdir(new_dir)
    yield
    os.chdir(previous_dir)

def check_num_parts(num_parts_arg, assignment_name):
    """Checks whether the grader has provided the number of parts
    for the assignment. If not, the grader is prompted for the
    number of parts.

    A valid number of parts is an integer > 1.

    Keyword arguments:
    num_parts_arg - the number of parts given as an argument
    assignment_name - the name of the assignment
    """

    err_msg = "That's not a valid number of parts."
    prompt = '> How many parts are in %s? ' % assignment_name
    if not num_parts_arg:
        while True:
            num_parts = int(input(prompt))
            if num_parts > 0:
                return num_parts
            else:
                log.warning(err_msg)
    elif num_parts_arg < 1:
        log.critical(err_msg + " Quitting program.")
        sys.exit(1)
    return num_parts_arg

def check_max_score(max_arg, assignment_name):
    """Checks whether the grader has provided the maximum score
    for the assignment. If not, the grader is prompted for the
    max score.

    A valid maximum score is a non-negative integer.
    """

    err_msg = "That's not a valid max_score."
    prompt = "> What's the maximum possible score for %s? " % assignment_name
    if not max_arg:
        while True:
            max_score = int(input(prompt))
            if max_score > 0:
                return max_score
            else:
                log.warning(err_msg)
    elif max_arg < 0:
        log.critical(err_msg + " Quitting program")
        sys.exit(1)
    return max_arg


def is_codecheck_file(f):
    """Checks if f is a codecheck file."""
    return f.endswith(codecheck_exts)


def verify_jar(jar_file):
    """Verify jar_file given by the filepath argument.

    Returns True if jar_file is signed correctly; False otherwise.
    """
    log.debug("Verifying {}...".format(jar_file))
    command = ['jarsigner', '-verify', jar_file]
    process = subprocess.Popen(command, stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE)

    output = process.communicate()  # a tuple (stdoutdata, stderrdata)
    return 'jar verified' in output[0].decode(encoding)


def get_filepaths_by_query(directory, query):
    """
    Gets the filepaths for the files thgat reside in directory
    whose name matches exactly with the query.
    
    Keyword Arguments:
    directory -- the directory to traverse
    query -- the name of the file to match
    """
    matched_files = []
    for dir_name, subdir_list, file_list in os.walk(directory):
        for file_name in file_list:
            if file_name == query:
                matched_files.append(dir_name + '/' + file_name)
    log.debug("The following hw files are found:")
    log.debug(pprint.pformat(matched_files))
    return matched_files


def get_dirpaths_by_query(directory, query):
    """
    Gets the filepaths for the files thgat reside in directory
    whose name matches exactly with the query.
    
    Keyword Arguments:
    directory -- the directory to traverse
    query -- the name of the file to match
    """
    matched_dirs = []
    for dir_name, subdir_list, file_list in os.walk(directory):
        for subdir in subdir_list:
            if subdir == query:
                matched_dirs.append(dir_name + "/" + subdir)
    log.debug(pprint.pformat(matched_dirs))
    return matched_dirs


def get_directory_path(filepath):
    """Gets the directory path from the filepath."""
    return os.path.dirname(filepath)


def get_filename(filepath):
    """Gets the filename from the filepath."""
    return os.path.basename(filepath)


def remove_ext(filename):
    """Returns a string with the filename without the extension.

    Sample output:
    remove_ext('hw1.zip') -> hw1
    remove_ext('Numbers.signed.zip') -> 'Numbers'
    remove_ext('SomeFile.a.crazy.file.extension') -> SomeFile
    """
    return filename[0:filename.index('.')]


def remove_directories(directories):
    """Remove the given list of directories."""
    for directory in directories:
        shutil.rmtree(directory, ignore_errors=True)
        

def remove_files(filepaths, key=None):
    """Removes the files given by the list of filepaths.

    If a key function is given, then the list of filepaths will
    be filtered down to results for which the key returns True."""
    for filepath in filter(key, filepaths):
        os.remove(filepath)

        
def remove_codecheck_files(directories):
    """Removes all the codecheck files found directory trees of
    the list of directories passed in.

    Keyword Arguments:
    directories - the directories to remove from."""
        
    for directory in directories:
        for root, dirs, files in os.walk(directory):
            with pushd(root):
                remove_files(files, key=is_codecheck_file)

                        
def get_extract_directories(hw_files):
    """Returns a list of directories that the given list
    of hw files would extract to."""
    dirname = remove_ext(get_filename(hw_files[0]))

    # All the hw directories that have been extracted
    return [get_directory_path(path) + "/" + dirname for path in hw_files]


def extract_codecheck_file(hw_filepath):
    """Extracts the report.html file from the codecheck file.

    The hw_filepath must point to a codecheck file.
    """
    log.info("Extracting codecheck file at {}".format(hw_filepath))

    # the directory name to extract the file to
    dirname = remove_ext(get_filename(get_filename(hw_filepath)))
    
    with zipfile.ZipFile(hw_filepath) as hw_zipfile:
        if 'report.html' in hw_zipfile.namelist():
            hw_zipfile.extract('report.html', dirname)


def extract_codecheck_files(hw_dirpaths, verify=True):
    """Extracts all the codecheck files specified by the
    list of homework directory paths.

    If verify is True, then the codecheck file will be checked
    by jarsigner before extracting the codecheck file. Otherwise,
    all the codecheck files will be extracted without verification."""
    # traverse all the homework directories
    for dirpath in hw_dirpaths:
        for root, dirs, files in os.walk(dirpath):

            # make the current directory the root of this walk
            with pushd(root):
                
                # look at codecheck files
                for f in filter(is_codecheck_file, files):
                    if verify:
                        if verify_jar(f):
                            extract_codecheck_file(os.path.abspath(f))
                        else:
                            unsigned_submissions.append(os.path.abspath(f))
                    else:  # extract without verification step
                        extract_codecheck_file(os.path.abspath(f))


def extract_homework_files(hw_filepaths):
    """Extracts all the homework files in the given list of hw_filepaths."""
    hw_directories = get_extract_directories(hw_filepaths)

    for hw_filepath, extract_dirpath in zip(hw_filepaths, hw_directories):
        log.info("Extracting {}".format(hw_filepath))
        with zipfile.ZipFile(hw_filepath) as hw_zipfile:
            hw_zipfile.extractall(extract_dirpath)


def calculate_scores(directories, number_parts, max_score):
    """Calculates the total scores for each student and saves
    it into the student_info dictionary."""
    
    def extract_re_group(regex_pat, text):
        """Extracts the first group from the regex_pat from text"""
        match = re.search(regex_pat, text)
        if match:
            return match.group(1)

    def extract_submission_id_from_report(report):
        """Extracts the submission ID from a codecheck report file read
        as a string."""
        return extract_re_group(r'<meta name="Submission" content="(\d+)"/>',
                                report)

    def extract_problem_id_from_report(report):
        """Extracts the problem ID from a codecheck report file read
        as a string."""
        return extract_re_group(r'<meta name="Problem" content="(\w+)"/>',
                                report)

    def extract_problem_level_from_report(report):
        """Extracts the problem level from a codecheck report file read
        as a string."""
        return extract_re_group(r'<meta name="Level" content="(\d)"/>', report)

    def extract_classname_from_report(report):
        """Extracts the Java class name from a codecheck report file read
        as a string."""
        pattern = (r'<p class="header">Student files</p>\s*'
                   r'<p class="caption">([a-zA-Z0-9_.]+):</p>')
        return extract_re_group(pattern, report)

    def extract_score_from_report(report):
        """Extracts the score from a codecheck report file read
        as a string."""
        score_pattern = r'<p class="score">((\d+/\d+)|(\d))</p>'
        return extract_re_group(score_pattern, report)

    def normalize_total(score):
        """Normalizes the total score based on the assignment max
        score and the number of parts for the assignment."""
        return int(score * max_score) / number_parts

    
    for directory in directories:
        student_name = os.path.basename(os.path.dirname(directory))
        total_score = Fraction()
        for root, dirs, files in os.walk(directory):
            with pushd(root):
                # log.debug("Now in %s", root)
                if 'report.html' in files:
                    # get the score from report.html and add it to the total
                    with codecs.open('report.html', 'r', 'utf-8') as f:
                        contents = f.read()
                        report_score = extract_score_from_report(contents)
                        log.debug("%s's %s report score = %s",
                                  student_name,
                                  os.path.basename(root),
                                  report_score)
                        total_score += Fraction(report_score)
        normalized_total = normalize_total(total_score)
        # log.debug("%s %s", directory, total_score)
        # log.debug("normalized = %s", normalize_total(total_score))
        student_info[student_name] = normalized_total

def create_assignment_report(course_path, hw_name, max_score):
    """Creates an aggregate assignment report named {hwfile}.txt"""
    def write_line(report, key, value):
        report.write("{} {}\n".format(key, value))
        
    def write_headers(report):
        write_line(report, ASSIGNMENT_NAME_KEY, hw_name)
        write_line(report, MAX_KEY, max_score)
        write_line(report, AVERAGE_KEY, mean(student_info.values()))
        write_line(report, STD_DEV_KEY, std_dev(student_info.values()))

    def write_separator(report):
        report.write(SEPARATOR)
        report.write("\n")

    def write_student(report, student, score):
        BLANK_LINES = "\n" * 5
        fraction_score = "{}/{} ({})".format(score, max_score,
                                             score_as_percentage(score, max_score))
        entries = OrderedDict(
            [(NAME_KEY, student),
             (SCORE_KEY, fraction_score),
             (COMMENT_KEY, BLANK_LINES),
             (EXTRA_KEY, BLANK_LINES)]
        )
        for key, value in entries.items():
            write_line(report, key, value)
        
    with pushd(course_path):
        with codecs.open("%s.txt" % remove_ext(hw_name), 'w', 'utf-8') as report:
            write_headers(report)
            write_separator(report)
            for student_name, score in student_info.items():
                write_student(report, student_name, score)
                write_separator(report)


def score_as_percentage(score, max_score_possible):
    """Returns the score as a percentage, in the String format xx.xx%."""
    return "%.2f%%" % ((float(score) / max_score_possible) * 100)


# -------------------- statistics functions --------------------

def mean(scores):
    """Returns the mean of the scores."""
    if len(scores) == 0:
        return
    return float(sum(scores)) / len(scores)


def variance(scores):
    """Returns the population variance of the scores."""
    # We're using population variance instead of sample variance
    # because we have all the assignment submissions to work with.
    if len(scores) == 0:
        return
    m = mean(scores)
    total = sum([(score - m)**2 for score in scores])
    return float(total) / len(scores)


def std_dev(scores):
    """Returns the standard deviation of the scores."""
    return math.sqrt(variance(scores))

# --------------------------------------------------------------

def init_logging(debug=False, filelog=False):
    """Initialize logging."""
    global log
    log = logging.getLogger(__name__)
    loglevel = logging.DEBUG if debug else logging.INFO

    log.setLevel(loglevel)

    # create console handler
    ch = logging.StreamHandler()
    ch.setLevel(loglevel)
    cformatter = logging.Formatter("%(message)s")
    ch.setFormatter(cformatter)
    log.addHandler(ch)

    if filelog:
        # create file handler which logs debug messages
        fh = logging.FileHandler('{}/ccgrade.log'.format(script_path()))
        fh.setLevel(logging.DEBUG)
        fformatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(fformatter)
        log.addHandler(fh)


def parse_args():
    """Create and return the argument parser."""
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--debug',
                        help="print more info to stdout for debugging purposes",
                        action='store_true')
    parser.add_argument('-n', '--number-parts',
                        help='provide the number of parts for this assignment',
                        type=int)
    parser.add_argument('-m', '--max-score',
                        help=("The maximum score possible for this assignment"),
                        type=int)
    parser.add_argument('--no-verify',
                        help=('skip jarsigner verification and unarchive '
                              'all submission files'),
                        action='store_true')
    #TODO: Add support for codecheck metadata checking
    # parser.add_argument('--no-checks',
    #                     help=("Don't check for warnings/cheating submissions "
    #                           "(checks by default)"),
    #                     action="store_true")
    parser.add_argument('--log',
                        help="Send log of the script execution to a file 'grade.log'.",
                        action="store_true")
    parser.add_argument('homework_file',
                        help=('The name of the homework file. Must match a homework '
                              'file ready to be graded in the student directories. e.g., hw1.zip'))
    parser.add_argument('course_path',
                        help=("the directory the student's homework submissions."))
    return parser.parse_args()


def script_path():
    """Returns the absolute path of this script"""
    pathname = os.path.dirname(sys.argv[0])
    return os.path.abspath(pathname)


def main():
    """The main function."""
    args = parse_args()
    init_logging(args.debug, args.log)
    number_parts = check_num_parts(args.number_parts, args.homework_file)
    max_score = check_max_score(args.max_score, args.homework_file)
    log.debug(number_parts)
    log.debug(max_score)

    # All the hw files in the course directory
    hw_filepaths = get_filepaths_by_query(args.course_path, args.homework_file)

    if not hw_filepaths:
        sys.exit("No files named {} were found.".format(args.homework_file))
    
    hw_directories = get_extract_directories(hw_filepaths)

    # remove_directories(get_extract_directories(hw_filepaths))

    # Step 1 -- Prepare files - extract all the homework zip files
    extract_homework_files(hw_filepaths)

    # Step 2 -- Verify and extract - extract all the report.html from the codecheck files
    extract_codecheck_files(hw_directories, verify=(not args.no_verify))

    # Step 3 -- Clean up - remove all the codecheck files
    remove_codecheck_files(hw_directories)

    # Step 4 -- Parse reports - parse the reports and total the scores
    calculate_scores(hw_directories, number_parts, max_score)

    # This is what the student assignment grades look like
    log.debug(pprint.pformat(student_info.items()))

    # Step 5 -- Create aggregate report
    create_assignment_report(args.course_path, args.homework_file, max_score)

    # Step 6 -- Clean up homework files - remove all the homework zip files
    remove_files(hw_filepaths)

    # We're done!

    if len(unsigned_submissions) == 0:
        log.info("All codecheck submissions passed jarsigner verification.")
    else:
        log.info("The following files are unverified by codecheck:")
        for submission in unsigned_submissions:
            log.info(submission)


if __name__ == "__main__":
    main()
