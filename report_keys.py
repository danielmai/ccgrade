# aggregate report file keys

## overall assignment keys
ASSIGNMENT_NAME_KEY = ":assignment name:"         # The name of this assignment
MAX_KEY             = ":max score possible:"      # The maximum points for this assignment
AVERAGE_KEY         = ":average:"                 # The average score for this assignment
STD_DEV_KEY         = ":standard deviation:"      # The standard deviation of the scores for this assignment

## student-specific keys
NAME_KEY            = ":name:"                    # The name of the student
SCORE_KEY           = ":total score:"             # The score the student received for the assignment
COMMENT_KEY         = ":comment:"                 # Comments for a student's submission on Canvas
EXTRA_KEY           = ":notes and score changes:" # Notes and score modifications

SEPARATOR           = "-" * 8
